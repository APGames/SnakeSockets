import socket
import threading
import socketserver
from SnakeClass.tools import *
from SnakeClass.Snake import Snake
from time import sleep


karta = None
count_screens = 0
count_screens_buff = 0
is_started = False
monitors_repaint = []
monitors_control = []


class ThreadedTCPRequestHandlerRepaint(socketserver.BaseRequestHandler):
    def setup(self):
        global count_screens_buff
        global tool
        global tool_buf
        self.id = count_screens_buff
        monitors_repaint.append(self)
        count_screens_buff += 1
        tool.set_area(Vector2d(0, 0, tool_buf), Vector2d(32 * count_screens_buff, 18, tool_buf))

    def handle(self):
        global snake
        global is_stop
        while is_stop:
            data = self.request.recv(1024).decode()
            if is_started:
                if data == "get_snake":
                    res = []
                    for i in snake.tail:
                        x, y = map(int, str(i).split())
                        x -= self.id * 32
                        res.append("{} {}".format(x, y))
                    x, y = map(int, str(snake.eat).split())
                    x -= self.id * 32
                    res.append("{} {}".format(x, y))
                    self.request.send('|'.join(res).encode())
                elif data == "close":
                    break
            else:
                self.request.send("no".encode())

    def finish(self):
        global count_screens_buff
        global monitors_control
        global tool
        global tool_buf
        count_screens_buff -= 1
        monitors_repaint.remove(self)
        for i in range(len(monitors_repaint)):
            monitors_repaint[i].id = i
        tool.set_area(Vector2d(0, 0, tool_buf), Vector2d(32 * count_screens_buff, 18, tool_buf))


class ThreadedTCPRequestHandlerControl(socketserver.BaseRequestHandler):
    def setup(self):
        global count_screens
        self.id = count_screens
        count_screens += 1
        monitors_control.append(self)

    def handle(self):
        global snake
        global is_stop
        while is_stop:
            data = self.request.recv(1024).decode()
            if data == "close":
                break
            if self.id == 0:
                for i in range(4):
                    if snake.direction == i and int(data) != (i + 2) % 4:
                        snake.next_direction = int(data)
                        break
            self.request.send("yes".encode())

    def finish(self):
        global count_screens
        global monitors_control
        count_screens -= 1
        monitors_control.remove(self)
        for i in range(len(monitors_control)):
            monitors_control[i].id = i


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


def get_host():
    sock = socket.socket()
    sock.connect(("google.com", 80))
    return sock.getsockname()


if __name__ == "__main__":
    print("Server open...")
    HOST, _ = get_host()
    serverRepaint = ThreadedTCPServer((HOST, 0), ThreadedTCPRequestHandlerRepaint)
    serverControl = ThreadedTCPServer((HOST, serverRepaint.server_address[1] + 1), ThreadedTCPRequestHandlerControl)
    threading.Thread(target=serverRepaint.serve_forever, daemon=True).start()
    threading.Thread(target=serverControl.serve_forever, daemon=True).start()
    print("Local server started!\nPassword: {0[0]}:{0[1]}".format(serverRepaint.server_address))
    tool = Tools()
    tool_buf = Tools()
    tool.set_area(Vector2d(0, 0, tool_buf), Vector2d(32 * max(count_screens, 1), 18, tool_buf))
    tool.set_cycle()
    snake = Snake(tool)
    is_stop = True
    s = input("Send 'start' command for continue. Other word for exit\n")
    if s == "start":
        if count_screens == 0:
            raise Exception("Zero connect monitors")
        is_started = True
        while is_stop:
            is_stop = snake.move()
            sleep(0.1)
